# Run

You can run it with 
```npm run start```

# Test

You can use 
``` npm run test ```
to run all tests and keep watching. 


All tests is api based and run on a real database, not mocked.

You can check details from database  access on ``` config/config.json ```

# Migrations

You can run migrations with



```
npm run db:runmigration
npm run db:makemigration
npm run db:runmigration:test

```


# Api documentation

API documentation will be running on http://localhost:3000/api-docs


