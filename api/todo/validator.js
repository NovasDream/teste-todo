const { check } = require('express-validator/check');

const createValidator = [
    check('message')
        .exists().withMessage('message is missing')
        .isLength({ min: 5 }).withMessage('is so small')
        .isLength({ max: 500 }).withMessage('is too bigger')
]
module.exports = {
    createValidator
}