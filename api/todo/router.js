var express = require('express');
var router = express.Router();

const {Todo} = require('../../models');

const {createValidator} = require('./validator');

/**
 * This route will create a new TODO based on your message.
 * @route POST /v1/todo
 * @param {ReqCreateTodoJSON.model} message.body.require -- it must have min 5, and max 500.
 * @returns {RetTodoJSON.model} 201 - will return the created todo with complete details from it.
 * @returns {RetErrorBaseJSON.model}  400 - This error will be based on name of expected field. - Unexpected error
 * @produces application/json
 * @consumes application/json
 */
var createTodo = (req, res, next) => {
    req.asyncValidationErrors().then(() => {
        const created = Todo.createFromBody(req.body)
        created.then((d) => {
            res.created(d);
        });
    }).catch((body) => {
        res.validationError(body);
    })
};

/**
 * This will return a list 10 TODO items. if you need more itens send created_at with date of first item.
 * @route GET /v1/todo
 * @param {date} created_at.query - used to search todo from date
 * @returns {Array.<RetTodoJSON>} 200 - will return a list of todos
 * @produces application/json
 */
var listTodo = (req, res, next) => {
    const startSearchOnDate = req.query.created_at;
    
    Todo.findAndCountAllWithCreated(startSearchOnDate).then((countedData) => {
        res.status(200).json({
            content : { count: countedData.count, rows: countedData.rows }
        });
    })
}

/**
 * This route will destroy a todo 
 * @route DELETE /v1/todo/:id
 * @param {integer} id.path.require - ID of todo to destroy
 * @returns {[object]} 200 - if todo can be destroyed
 * @returns {Error}  404 - not found
 * @produces application/json
 */
var deleteTodo = (req, res, next) => {
    Todo.destroy({
        where: {id : req.params.id}
    }).then((impactedResults) => {
        if (impactedResults > 0) {
            res.status(200).send();
        } else {
            res.notFound();
        }
    });
}

router.post('/', createValidator, createTodo);
router.get('/', listTodo);
router.delete('/:id', deleteTodo);

module.exports = router;

/**
 * JSON parameters require a model. This one just has "name"
 * @typedef ReqCreateTodoJSON
 * @property {string} message.required 
 */

