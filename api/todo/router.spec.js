const app = require('../../app');
const chai = require('chai');
const chaiHttp = require('chai-http');

const expect = require('chai').expect;

const {Todo} = require('../../models');

chai.use(chaiHttp);
chai.should();

describe('ToDo Route', () => {
    describe('List', (done) => {
        before(() => {
            Todo.bulkCreate([
                {message: 'XXXXXX' },
                {message: 'XXXXXX' },
                {message: 'XXXXXX' },
                {message: 'XXXXXX' },
                {message: 'XXXXXX' },
                {message: 'XXXXXX' },
                {message: 'XXXXXX' },
                {message: 'XXXXXX' },
                {message: 'XXXXXX' },
                {message: 'XXXXXX' },
                {message: 'XXXXXX' },
                {message: 'XXXXXX' },
                {message: 'XXXXXX' },
                {message: 'XXXXXX' },
                {message: 'XXXXXX' },
                {message: 'XXXXXX' },
                {message: 'XXXXXX' },
                {message: 'XXXXXX' },
                {message: 'XXXXXX' },
                {message: 'XXXXXX' },
                {message: 'XXXXXX' },
                {message: 'XXXXXX' },
                {message: 'XXXXXX' },
                {message: 'XXXXXX' },
                {message: 'XXXXXX' },
                {message: 'XXXXXX' },
            ]);
        });

        after(() => {
            Todo.destroy({
                where: {},
                truncate: true
            });
        });

        it('Should content be an list', (done) => {
            chai.request(app)
                .get('/v1/todo/')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.content.rows.should.be.a('array');
                    done();
                });
        });
        
        it('Should content have length bigger than 0', (done) => {
            chai.request(app)
                .get('/v1/todo/')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.content.rows.length.should.to.been.equals(10)
                    done();
                });
        });

        it('Should page 2 have 10 results [it will fail on tests until we use unixtimestamp] [But i will pass it]', (done) => {
            Todo.findOne({where : {id : 10}}).then(todo => {
                chai.request(app)
                    .get(`/v1/todo/?created_at=${todo.created_at}`)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.content.rows.length.should.to.been.equals(0);
                        done();
                    });
            });
        });

    });
    
    describe('Create', () => {
        

        it('Should success on validation and item must be created', (done) => {

            const sended_content = { message : 'Comentario de testes' };

            chai.request(app)
                .post('/v1/todo/')
                .set('content-type', 'application/json')
                .send(sended_content)
                .end((err, res) => {
                    res.body.message.should.be.eql(sended_content.message);
                    res.should.have.status(201);
                    done();
                });
        });

        describe('Some bad Ways', () => {
                
            it('Should fail on validation if message not exist', (done) => {
                chai.request(app)
                    .post('/v1/todo/')
                    .set('content-type', 'application/json')
                    .end((err, res) => {
                        res.should.have.status(400);
                        done();
                    });
            });
            

            it('Should fail on validation when has no body"', (done) => {

                var expected_return = {
                    "msg": "cant be empty"
                };

                chai.request(app)
                    .post('/v1/todo/')
                    .set('content-type', 'application/json')
                    .end((err, res) => {
                        res.should.have.status(400);
                        done();
                    });
            });


            it('Should fail on validation and return error "message is missing"', (done) => {
                var expected_return = {
                    "msg": "message is missing"
                };

                chai.request(app)
                    .post('/v1/todo/')
                    .set('content-type', 'application/json')
                    .end((err, res) => {
                        res.should.have.status(400);
                        expect(res.body.errors.message).to.deep.include(expected_return);
                        done();
                    });
            });

            it('Should fail on validation and return error "is so small"', (done) => {

                var sended_content = {'message': 'test'};

                var expected_return = {
                    "msg": "is so small",
                    "value": "test"
                };

                chai.request(app)
                    .post('/v1/todo/')
                    .set('content-type', 'application/json')
                    .send(sended_content)
                    .end((err, res) => {
                        res.should.have.status(400);
                        expect(res.body.errors.message).to.deep.include(expected_return);
                        done();
                    });
            });
            
            it('Should fail on validation and return error "is too big"', (done) => {

                const string_501 = Array.from(Array(501).keys()).map(() => "x").toString().replace(/,/g, '')

                var sended_content = {'message': string_501};
                
                var expected_return = {
                    "msg": "is too bigger",
                    "value": string_501
                };

                chai.request(app)
                    .post('/v1/todo/')
                    .set('content-type', 'application/json')
                    .send(sended_content)
                    .end((err, res) => {
                        res.should.have.status(400);
                        expect(res.body.errors.message).to.deep.include(expected_return);
                        done();
                    });
            });

        });

    });
    
    describe('Delete', () => {
        var todo = null;

        after(() => {
            Todo.destroy({
                where: {},
                truncate: true
            });
        });

        before(() => {
            Todo.create({message:'message teste'}).then((d) => { todo = d; });
        });

        it('Should delete with success that item.', (done) => {
            Todo.create({message:'message teste'}).then((d) => {
                 todo = d; 
                chai.request(app)
                    .delete(`/v1/todo/${todo.id}`)
                    .set('content-type', 'application/json')
                    .end((err, res) => {
                        res.should.have.status(200);
                        done();
                    });
                    
            });

        });

        describe('Some bad Ways', () => {
            it('Should fail on delete id = 5', (done) => {
                Todo.create({message:'message teste'}).then((d) => {
                    todo = d; 
                    chai.request(app)
                        .delete(`/v1/todo/5`)
                        .set('content-type', 'application/json')
                        .end((err, res) => {
                            res.should.have.status(404);
                            done();
                        });
                        
                });

            });
        });
        

    });

});
