"use strict";

/**
 * JSON parameters require a model. This one just has "name"
 * @typedef RetTodoJSON
 * @property {integer} id 
 * @property {string} message 
 * @property {string} created_at - datetime
 * @property {string} updated_at - datetime
 */
module.exports = function (DB, { INTEGER, BIGINT, DATE, STRING, ENUM, BOOLEAN, DATEONLY, NOW }) {
    const Model = DB.define('Todo',
      {
        id:      { type: INTEGER,  allowNull: false, primaryKey: true, autoIncrement: true },
        message: { type: STRING,   allowNull: false, validate: { notEmpty: true, len: [5, 500], },},
      },
      {
        timestamps: true,
        underscored: true,
        tableName: 'todos'
      }
    )

    Model.createFromBody = (body) => {
      const data = {
        message : body.message
      };

      return Model.create(data)
    }
  
    
    Model.findAndCountAllWithCreated = (date) => {
      let where = {};
      if (date)  { where.created_at = { [DB.Op.gt] : new Date(date) }; }

      return Model.findAndCountAll({
        where : where,
        limit: 10
      })
    }
    return Model
  }
