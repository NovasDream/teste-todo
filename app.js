var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var ExpressValidator = require('express-validator');
var cors = require('cors')

var { badRequest, notFound, created, validationError } = require('./middlewares/httpStatus');

var todoRouter  = require('./api/todo/router');

var app = express();

const expressSwagger = require('express-swagger-generator')(app);
let options = {
  swaggerDefinition: {
      info: {
          description: 'Swagger base server.',
          title: 'Swagger',
          version: '1.0.0',
      },
      host: 'localhost:3000',
      basePath: '/v1',
      produces: [
          "application/json"
      ],
      schemes: ['http'],
  },
  basedir: __dirname, //app absolute path
  files: [,
    './utils/*.js',
    './models/*.js',
    './api/**/*.js'
  ] //Path to the API handle folder
};
expressSwagger(options)

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// on test enviorement we dont need show logs from api
if(app.get('env') !== 'test') app.use(logger('dev'));
app.use(cors())
app.use(express.json());
app.use(ExpressValidator());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(badRequest);
app.use(notFound);
app.use(created);
app.use(validationError);



app.use('/v1/todo', todoRouter);


app.get('/', function (req, res) {
  res.render('index', { title: 'Hey', message: 'Hello there!'});
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
