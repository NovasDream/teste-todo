'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * addColumn "updated_at" to table "todos"
 * addColumn "created_at" to table "todos"
 *
 **/

var info = {
    "revision": 2,
    "name": "noname",
    "created": "2019-03-02T06:07:44.238Z",
    "comment": ""
};

var migrationCommands = [{
        fn: "addColumn",
        params: [
            "todos",
            "updated_at",
            {
                "type": Sequelize.DATE,
                "field": "updated_at",
                "allowNull": false
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "todos",
            "created_at",
            {
                "type": Sequelize.DATE,
                "field": "created_at",
                "allowNull": false
            }
        ]
    }
];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
