'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * createTable "todos", deps: []
 *
 **/

var info = {
    "revision": 1,
    "name": "noname",
    "created": "2019-03-01T11:40:50.358Z",
    "comment": ""
};

var migrationCommands = [{
    fn: "createTable",
    params: [
        "todos",
        {
            "id": {
                "type": Sequelize.INTEGER,
                "field": "id",
                "autoIncrement": true,
                "primaryKey": true,
                "allowNull": false
            },
            "message": {
                "type": Sequelize.STRING,
                "field": "message",
                "validate": {
                    "notEmpty": true,
                    "len": [5, 500]
                },
                "allowNull": false
            }
        },
        {}
    ]
}];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
