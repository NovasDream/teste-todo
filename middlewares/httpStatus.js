const {ApiError} = require('../utils');

function groupBy(xs, key) {
    return xs.reduce(function(rv, x) {
      (rv[x[key]] = rv[x[key]] || []).push({ msg: x.msg, value: x.value });
      return rv;
    }, {});
};


function badRequest(req, res, next) {
    res.badRequest = function(object) {
      var jsonObject = JSON.parse(JSON.stringify(object));
      res.status(400).json({errors: jsonObject});
    }
    next();
}

function validationError(req, res, next) {
    res.validationError = (validationError) => {
        const apiError = new ApiError({
            type:       'ValidationError',
            message:    'Algum dos dados entrados não são atendem a validação',
            devMessage: 'Existem campos com os dados invalidos ou faltando. mais detalhes em `errors`',
            errors:     groupBy(validationError, 'param')
        })
        // errorInput.error = groupBy(validationError, 'param');
        // var jsonObject = JSON.parse(JSON.stringify(error));
        // jsonObject.validationErrors = groupBy(validationError, 'param');
        res.status(400).json(apiError);
    }
    next();
}

function notFound(req, res, next) {
    res.notFound = function() {
      res.status(404).json();
    }
    next();
}

function created(req, res, next) {
    res.created = function(object) {
      res.status(201).json(object);
    }
    next();
}

module.exports = {
    badRequest,
    notFound,
    created,
    validationError
}
