/**
 * JSON parameters require a model. This one just has "name"
 * @typedef RetErrorBaseJSON
 * @property {string} type 
 * @property {string} message 
 * @property {string} devMessage - datetime
 * @property {Array.<RetFieldErrorJSON>} errors - datetime
 */

/**
 * JSON parameters require a model. This one just has "name"
 * @typedef RetFieldErrorJSON
 * @property {Array.<RetValidationErrorJSON>} namedFieldWithError
 */

/**
 * JSON parameters require a model. This one just has "name"
 * @typedef RetValidationErrorJSON
 * @property {string} message 
 * @property {string} value
 */
class ApiError {

    constructor({type, message, devMessage, errors}) {
        this.type = type;
        this.message = message; 
        this.devMessage = devMessage;
        this.errors = errors;
    };

}

module.exports = {ApiError};